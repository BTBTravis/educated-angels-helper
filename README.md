# Educated Angles Helper 
Small Node.js app responsible for retrieving info to be displayed on educatedangels.org


### Tech
* node js
* express.js
* webpack
* axios
* dotevn
* fb - facebook javascript sdk
* aws-sdk - amazon web services javascript sdk
* nedb - flatfile db

### Intergratoins
* signupgenius events
* facebook events
* facebook photos
* s3 bucket storage
