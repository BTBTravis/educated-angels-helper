const restify = require('restify');
const moment = require('moment');
const axios = require('axios');
const fb = require('fb');
const SignupGenius = require('./models/SignupGenius');
const FacebookEvents = require('./models/FacebookEvents');

const server = restify.createServer();

// Debugging Tool
// const util = require('util');
// console.log(util.inspect(util, { showHidden: true, depth: null }));

// Dependency Injection Container
const di = {
    moment,
    axios,
    fb,
    keys: {
        signupgenius: process.env.SIGNUPGENIUS_KEY,
        facebook: process.env.FACEBOOK_KEY,
    },
};

// Routing + Controllers
const signupGenius = new SignupGenius(di);
server.get('/signupgenius', (req, res, next) => {
    signupGenius.getEvents().then(events => {
        res.send(events);
        next();
    });
});

const faceBookEvents = new FacebookEvents(di);
server.get('/facebook/events', (req, res, next) => {
    faceBookEvents.getEvents().then(events => {
        res.send(events);
        next();
    })
        .catch(e => {
            res.send(new Error("Can't retrieve facebook events"));
            next();
        });
});

// Server Initialization
const port = process.env.PORT || '3000';
server.listen(port, function() {
    console.log('%s listening at %s', server.name, server.url);
});
