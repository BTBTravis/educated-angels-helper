class FacebookEvents {
    constructor(di) {
        this.container = di;
        this.lastRequest = 0;
        this.cacheLength = 1800;
        this.container.fb.setAccessToken(this.container.keys.facebook);
    }

    /**
     * Get events with cache layer
     * @returns {Promise}
     */
    getEvents() {
        const now = Math.floor(Date.now()/1000);
        if (now > this.lastRequest + this.cacheLength) {
            return this._getEvents().then(data => {
                this.lastRequest = now;
                this.cache = data;
                return data;
            });
        } else {
            return new Promise((resolve) => resolve(this.cache));
        }
    }

    /**
     * Get events from facebook
     * @returns {Promise}
     * @private
     */
    _getEvents() {
        return new Promise((resolve, reject) => {
            this.container.fb.api(
                '/educatedangels/events',
                'GET',
                { 'time_filter': 'upcoming' },
                (res) => {
                    if (!res || res.error) {
                       return  reject(res.error);
                    }
                    const events = res.data.map(evt => {
                        evt.event_type = 'facebook';
                        evt.month = this.container.moment(evt.start_time).format('MMM');
                        evt.day = this.container.moment(evt.start_time).format('D');
                        evt.unix = this.container.moment(evt.start_time).format('X');
                        evt.url = `https://www.facebook.com/events/${evt.id}/`;
                        return evt;
                    });
                    return resolve(events);
                });
        });
    }
}

module.exports = FacebookEvents;
