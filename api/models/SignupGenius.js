'use strict';

class SignupGenius {
    constructor(di) {
        this.container = di;
        this.lastRequest = 0;
        this.cacheLength = 1800;
    }

    getEvents() {
        const now = Math.floor(Date.now()/1000);
        if (now > this.lastRequest + this.cacheLength) {
            this.lastRequest = now;
            return this.container.axios.get('https://api.signupgenius.com/v2/k/signups/created/active/?user_key=' + this.container.keys.signupgenius)
                .then((response) => {
                    let events =  response.data.data.map(function (evt) {
                        evt.event_type = 'signupgenius';
                        evt.month = this.container.moment(evt.starttime).format('MMM');
                        evt.day = this.container.moment(evt.starttime).format('D');
                        return evt;
                    });
                    this.cache = events;
                    return events;
                });
        } else {
            return new Promise((resolve) => resolve(this.cache));
        }
    };
}

module.exports = SignupGenius;

